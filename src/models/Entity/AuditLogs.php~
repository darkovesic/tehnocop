<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * AuditLogs
 *
 * @ORM\Table(name="audit_logs", indexes={@ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="timestamp", columns={"timestamp", "modified_entity_name", "modified_entity_id", "operation"})})
 * @ORM\Entity
 */
class AuditLogs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="modified_entity_name", type="string", length=255, nullable=false)
     */
    private $modifiedEntityName;

    /**
     * @var int
     *
     * @ORM\Column(name="modified_entity_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $modifiedEntityId;

    /**
     * @var string
     *
     * @ORM\Column(name="operation", type="string", length=0, nullable=false, options={"default"="CREATE"})
     */
    private $operation = 'CREATE';

    /**
     * @var string|null
     *
     * @ORM\Column(name="old_data", type="blob", length=0, nullable=true)
     */
    private $oldData;

    /**
     * @var string|null
     *
     * @ORM\Column(name="new_data", type="blob", length=0, nullable=true)
     */
    private $newData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=false)
     */
    private $timestamp;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modifiedEntityName.
     *
     * @param string $modifiedEntityName
     *
     * @return AuditLogs
     */
    public function setModifiedEntityName($modifiedEntityName)
    {
        $this->modifiedEntityName = $modifiedEntityName;

        return $this;
    }

    /**
     * Get modifiedEntityName.
     *
     * @return string
     */
    public function getModifiedEntityName()
    {
        return $this->modifiedEntityName;
    }

    /**
     * Set modifiedEntityId.
     *
     * @param int $modifiedEntityId
     *
     * @return AuditLogs
     */
    public function setModifiedEntityId($modifiedEntityId)
    {
        $this->modifiedEntityId = $modifiedEntityId;

        return $this;
    }

    /**
     * Get modifiedEntityId.
     *
     * @return int
     */
    public function getModifiedEntityId()
    {
        return $this->modifiedEntityId;
    }

    /**
     * Set operation.
     *
     * @param string $operation
     *
     * @return AuditLogs
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation.
     *
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set oldData.
     *
     * @param string|null $oldData
     *
     * @return AuditLogs
     */
    public function setOldData($oldData = null)
    {
        $this->oldData = $oldData;

        return $this;
    }

    /**
     * Get oldData.
     *
     * @return string|null
     */
    public function getOldData()
    {
        return $this->oldData;
    }

    /**
     * Set newData.
     *
     * @param string|null $newData
     *
     * @return AuditLogs
     */
    public function setNewData($newData = null)
    {
        $this->newData = $newData;

        return $this;
    }

    /**
     * Get newData.
     *
     * @return string|null
     */
    public function getNewData()
    {
        return $this->newData;
    }

    /**
     * Set timestamp.
     *
     * @param \DateTime $timestamp
     *
     * @return AuditLogs
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp.
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set user.
     *
     * @param \Users|null $user
     *
     * @return AuditLogs
     */
    public function setUser(\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Users|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
