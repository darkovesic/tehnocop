<?php

namespace Vemid\ProjectOne\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class Users
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password_hash", type="string", length=255, nullable=false)
     */
    private $passwordHash;

    /**
     * @var string|null
     *
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gender", type="string", length=0, nullable=true)
     */
    private $gender;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_ip", type="string", length=255, nullable=true)
     */
    private $lastIp;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="registered_datetime", type="datetime", nullable=true)
     */
    private $registeredDatetime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_visit_datetime", type="datetime", nullable=true)
     */
    private $lastVisitDatetime;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return Users
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return Users
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return Users
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set passwordHash.
     *
     * @param string $passwordHash
     *
     * @return Users
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    /**
     * Get passwordHash.
     *
     * @return string
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * Set avatar.
     *
     * @param string|null $avatar
     *
     * @return Users
     */
    public function setAvatar($avatar = null)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar.
     *
     * @return string|null
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set gender.
     *
     * @param string|null $gender
     *
     * @return Users
     */
    public function setGender($gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return string|null
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return Users
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set lastIp.
     *
     * @param string|null $lastIp
     *
     * @return Users
     */
    public function setLastIp($lastIp = null)
    {
        $this->lastIp = $lastIp;

        return $this;
    }

    /**
     * Get lastIp.
     *
     * @return string|null
     */
    public function getLastIp()
    {
        return $this->lastIp;
    }

    /**
     * Set registeredDatetime.
     *
     * @param \DateTime|null $registeredDatetime
     *
     * @return Users
     */
    public function setRegisteredDatetime($registeredDatetime = null)
    {
        $this->registeredDatetime = $registeredDatetime;

        return $this;
    }

    /**
     * Get registeredDatetime.
     *
     * @return \DateTime|null
     */
    public function getRegisteredDatetime()
    {
        return $this->registeredDatetime;
    }

    /**
     * Set lastVisitDatetime.
     *
     * @param \DateTime|null $lastVisitDatetime
     *
     * @return Users
     */
    public function setLastVisitDatetime($lastVisitDatetime = null)
    {
        $this->lastVisitDatetime = $lastVisitDatetime;

        return $this;
    }

    /**
     * Get lastVisitDatetime.
     *
     * @return \DateTime|null
     */
    public function getLastVisitDatetime()
    {
        return $this->lastVisitDatetime;
    }
}
